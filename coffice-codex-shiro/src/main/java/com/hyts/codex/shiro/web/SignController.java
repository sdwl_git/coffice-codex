/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.shiro.web;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hyts.codex.base.DefaultBeanHandler;
import com.hyts.codex.base.dto.ResultDTO;
import com.hyts.codex.base.vo.ResultVO;
import com.hyts.codex.config.DataStatusEnum;
import com.hyts.codex.config.ResultTypeEnum;
import com.hyts.codex.service.impl.UserService;
import com.hyts.codex.shiro.service.AuthorHelper;
import com.hyts.codex.shiro.service.PasswordHelper;
import com.hyts.codex.user.bo.UserBO;
import com.hyts.codex.user.dto.LoginDTO;
import com.hyts.codex.user.dto.RegisterDTO;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 */
@RestController
@RequestMapping("/access")
public class SignController {
    
    
    @Resource(name="userService")
    UserService service;
    
    
    @Autowired
    PasswordHelper helper;
    
    /**  
     * <b>register</b>
     * <p>注册功能</p>
     * @param registerDTO
     * @return
     * @exception
     */ 
    @RequestMapping("/register")
    public ResultDTO<ResultVO<UserBO>> register(@RequestBody RegisterDTO registerDTO){
        UserBO userBO = new UserBO();
        userBO.setUserName(registerDTO.getUserName());
        userBO.setPassword(registerDTO.getPassword());
        userBO.setRealName(registerDTO.getRealName());
        userBO.setEmail(registerDTO.getEmail());
        userBO.setIsDelete(DataStatusEnum.NO_DEL.getCode());
        userBO.setStatus(DataStatusEnum.NORMAL.getCode());
        userBO.setLastLoginDate(new Date());
        helper.encryptPassword(userBO);
        return service.register(userBO);
    }
    
    
    /**  
     * <b>login</b>
     * <p>登录功能</p>
     * @param loginDTO
     * @return
     * @exception
     */ 
    @RequestMapping("/login")
    public ResultDTO<ResultVO<UserBO>> login(@RequestBody LoginDTO loginDTO){
        ResultDTO<ResultVO<UserBO>> result = DefaultBeanHandler.getINSTANCE().newResultDTO(UserBO.class);
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(AuthorHelper.build(loginDTO));
            if(subject.isAuthenticated()) {
                return result.success();
            }
        } catch (Exception e) {
            if(e instanceof IncorrectCredentialsException) {
               return result.failure(ResultTypeEnum.PASSWORD_NOPASS);
            }else if(e instanceof UnknownAccountException) {
                return result.failure(ResultTypeEnum.USER_NOFOUND);
            }else if(e instanceof LockedAccountException) {
                return result.failure(ResultTypeEnum.ACCOUNT_LOCK);
            }else {
            }
        }
        return result.failure("系统异常，登录失败！");
    }
}
