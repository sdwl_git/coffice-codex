/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.shiro.service;

import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.stereotype.Component;

import com.hyts.codex.shiro.config.ShiroConfiguration;
import com.hyts.codex.user.bo.UserBO;


/** 
 * @author LiBo/Alex
 * @version V1.0 
 */
@Component
public class PasswordHelper {
    
    private RandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();
    
    private String algorithmName = ShiroConfiguration.DEFAULT_SECRET_TYPE;
    
    private int hashIterations = ShiroConfiguration.DEFAULT_SECRET_NUM;
    
    public void setRandomNumberGenerator(RandomNumberGenerator randomNumberGenerator) {
        this.randomNumberGenerator = randomNumberGenerator;
    }
    public void setAlgorithmName(String algorithmName) {
        this.algorithmName = algorithmName;
    }
    public void setHashIterations(int hashIterations) {
        this.hashIterations = hashIterations;
    }
    
    public void encryptPassword(UserBO user) {
        user.setSalt(randomNumberGenerator.nextBytes().toHex());
        String oldPassword = user.getPassword();
        String newPassword = new SimpleHash(
                //加密算法
                algorithmName,
                //密码
                oldPassword,
                //盐值 username+salt
                ByteSource.Util.bytes(user.getUserName()+user.getSalt()),
                hashIterations//hash次数
        ).toHex();
        user.setPassword(newPassword);
    }
    
}
