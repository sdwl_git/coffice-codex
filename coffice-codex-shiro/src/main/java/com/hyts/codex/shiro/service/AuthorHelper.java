/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.shiro.service;

import java.util.Objects;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;

import com.hyts.codex.tool.NetworkUtils;
import com.hyts.codex.user.dto.LoginDTO;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 */
public class AuthorHelper {
    
    /**  
     * @param dto
     * @return
     * @exception
     */ 
    public static AuthenticationToken build(LoginDTO dto) {
        if(Objects.nonNull(dto)) {
            UsernamePasswordToken token = new UsernamePasswordToken();
            token.setHost(NetworkUtils.getServerIpAddress());
            token.setRememberMe(dto.isRememberMe());
            token.setUsername(dto.getUserName());
            token.setPassword(dto.getPassword().toCharArray());
            return token;
        }
        return null;
    }
}   
