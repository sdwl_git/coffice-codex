/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.app.sign.web;

import java.io.IOException;
import java.text.MessageFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/** 
 * @author LiBo/Alex
 * @since V1.0
 * @version V1.0 
 */
@Controller
public class SignViewController extends HttpServlet{
    
    
    private static final String DEFAULT_LOGIN_HTML_NAME = "sign.html";
    
    private static final String DEFAULT_LOGIN_HTML_PATH = "/stateless/";
    
    private static final String DEFAULT_LOGIN_PATH = "login";
    
    private static final int DEFAULT_LOGIN_HTML_INDEX = 1;
    
    private static final int DEFAULT_LOGIN_MAX_INDEX = 5;

    /**  
     * @fieldName serialVersionUID
     * @fieldType long
     */ 
    private static final long serialVersionUID = 1L;
    


    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getRequestURI().replace(DEFAULT_LOGIN_PATH,"");
        int indexLogin = DEFAULT_LOGIN_HTML_INDEX;
        indexLogin = indexLogin > DEFAULT_LOGIN_MAX_INDEX ? DEFAULT_LOGIN_HTML_INDEX:indexLogin;
        req.getRequestDispatcher(path+DEFAULT_LOGIN_HTML_NAME).
            forward(req, resp);
    }


    @RequestMapping("/access/{index}")
    public String login(HttpServletRequest request,HttpServletResponse response,@PathVariable
    int index) {
        return MessageFormat.format("redirect:/stateless/{0}/login", index);
    }

}
