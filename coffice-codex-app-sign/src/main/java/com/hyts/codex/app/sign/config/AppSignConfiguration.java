/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.app.sign.config;

import java.util.ArrayList;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.hyts.codex.app.sign.web.SignViewController;
import com.hyts.codex.config.ImportAuth;
import com.hyts.codex.shiro.annotation.ImportShiro;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 */
@ImportAuth
@ImportShiro
@Configuration
public class AppSignConfiguration {
    
    @Bean
    public ServletRegistrationBean<SignViewController> signServletBean() {
        ServletRegistrationBean<SignViewController> bean = new ServletRegistrationBean<SignViewController>();
        bean.setEnabled(true);
        bean.setAsyncSupported(true);
        bean.setServlet(new SignViewController());
        bean.setName("signServlet");
        bean.addUrlMappings("/stateless/1/login",
                            "/stateless/2/login",
                            "/stateless/3/login",
                            "/stateless/4/login",
                            "/stateless/5/login",
                            "/stateless/6/login");
        return bean;
    }   

}
