/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.app.sign;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import com.hyts.codex.app.sign.config.ImportAppSign;
import com.hyts.codex.config.ImportAuth;
import com.hyts.codex.shiro.annotation.ImportShiro;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 */

//@ImportAppSign
@SpringBootApplication
public class AppSignApplication {
    
    public static void main(String[] args) {
        new SpringApplicationBuilder(AppSignApplication.class)
        .web(WebApplicationType.SERVLET).run(args);
    }
    
}
