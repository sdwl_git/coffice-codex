/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.error;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 * 框架级别操作任务服务异常信息
 */
abstract class FrameworkError extends RuntimeException {

    /**  
     * @fieldName serialVersionUID
     * @fieldType long
     */ 
    private static final long serialVersionUID = 1L;
    
    /**  
     * @constructor：FrameworkError  
     * @param message
     * @param cause  
     */ 
    public FrameworkError(String message, Throwable cause) {
        super(message, cause);
    }
    /**  
     * @constructor：FrameworkError  
     * @param message  
     */ 
    public FrameworkError(String message) {
        super(message);
    }
    /**  
     * @constructor：FrameworkError  
     * @param cause  
     */ 
    public FrameworkError(Throwable cause) {
        super(cause);
    }
    
}
