/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.error;


import com.hyts.codex.config.ModuleTypeEnum;
import com.hyts.codex.config.ModuleTypeEnum.OPRErrorType;

import lombok.Getter;


/** 
 * @author LiBo/Alex
 * @version V1.0
 * 基础抽象操作错误类
 */
public class OperationError extends FrameworkError {
    
    /**  
     * @fieldName serialVersionUID
     * @fieldType long
     */ 
    private static final long serialVersionUID = 1L;

    @Getter
    private ModuleTypeEnum.OPRErrorType operationErrorType;
    
    @Getter
    private ModuleTypeEnum.LAYERErrorType layerErrorType;

    /**  
     * @constructor：ControllerError  
     * @param message
     * @param cause
     * @param operationErrorType
     */
    public OperationError(String message, Throwable cause, OPRErrorType operationErrorType, ModuleTypeEnum.LAYERErrorType layerError) {
        super(message, cause);
        this.operationErrorType = operationErrorType;
        this.layerErrorType = layerError;
    }
    
    /**
     * @constructor：ControllerError  
     * @param message
     * @param operationErrorType
     */
    public OperationError(String message, OPRErrorType operationErrorType, ModuleTypeEnum.LAYERErrorType layerError) {
        super(message);
        this.operationErrorType = operationErrorType;
        this.layerErrorType = layerError;
    }
    
    /**
     * @constructor：ControllerError  
     * @param operationErrorType
     */
    public OperationError(Throwable cause, OPRErrorType operationErrorType, ModuleTypeEnum.LAYERErrorType layerError) {
        super(cause);
        this.operationErrorType = operationErrorType;
        this.layerErrorType = layerError;
    }
}
