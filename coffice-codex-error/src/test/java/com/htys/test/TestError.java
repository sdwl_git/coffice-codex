/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.htys.test;

import com.hyts.codex.config.ModuleTypeEnum.LAYERErrorType;
import com.hyts.codex.config.ModuleTypeEnum.OPRErrorType;
import com.hyts.codex.error.OperationError;

/** 
 * @author LiBo/Alex
 * @see TODO制定另外一个主体的链接
 * @since TODO引入特定改变发布版本
 * @version V1.0 
 */
public class TestError extends OperationError {

    /**  
     * @fieldName serialVersionUID
     * @fieldType long
     */ 
    private static final long serialVersionUID = 1894256224161724220L;

    /**  
     * @constructor：TestError  
     * @param message
     * @param operationErrorType
     * @param layerError  
     */ 
    public TestError(String message, OPRErrorType operationErrorType, LAYERErrorType layerError) {
        super(message, operationErrorType, layerError);
    }

    public TestError(String message, Throwable cause, OPRErrorType operationErrorType, LAYERErrorType layerError) {
        super(message, cause, operationErrorType, layerError);
    }

    public TestError(Throwable cause, OPRErrorType operationErrorType, LAYERErrorType layerError) {
        super(cause, operationErrorType, layerError);
    }

}
