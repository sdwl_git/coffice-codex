/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.aspectj.interceptor;

import com.hyts.codex.aspectj.annotation.validate.ValidateScope;
import com.sun.javafx.tools.packager.Param;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * @project-name:coffice-codex
 * @package-name:com.hyts.codex.aspectj.interceptor
 * @author:LiBo/Alex
 * @create-date:2019-10-26 12:16
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@Slf4j
@Aspect
@Component
public class ValidateInterceptor {


    @Around("@annotation(valid)")
    public void validate(ProceedingJoinPoint proceedingJoinPoint, ValidateScope valid){
        Object[] args = proceedingJoinPoint.getArgs();
        Arrays.asList(args).stream().map(param->param.getClass()).
                forEach(param->{
            Pattern pattern = param.getAnnotation(Pattern.class);
                    Class clazz = pattern.getClass();
                    System.out.println(clazz.getDeclaredFields());
         });
    }

}
