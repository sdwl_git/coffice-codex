/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.aspectj.interceptor;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 * @project-name:coffice-codex
 * @package-name:com.hyts.codex.aspectj.interceptor
 * @author:LiBo/Alex
 * @create-date:2019-10-25 00:02
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
public interface BaseInterceptor<P>{

    /**
     * 切面
     */
    void pointCut(P param);

    /**
     * 前置
     */
    void preHandle(JoinPoint joinPoint,P pre);

    /**
     * 后置
     */
    void postHandle(JoinPoint joinPoint,P param);

    /**
     * 环绕
     * @param proceedingJoinPoint
     */
    void include(ProceedingJoinPoint proceedingJoinPoint);

    /**
     * return 返回处理
     * @param joinPoint
     */
    void returnHandle(JoinPoint joinPoint);

    /**
     * error 错误处理
     * @param joinPoint
     */
    void errorHandle(JoinPoint joinPoint);


}
