/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.aspectj.interceptor;

import com.hyts.codex.aspectj.annotation.http.HttpAnchor;
import com.hyts.codex.aspectj.annotation.http.HttpRequestData;
import com.hyts.codex.aspectj.annotation.http.HttpResponseData;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
 * @project-name:coffice-codex
 * @package-name:com.hyts.codex.aspectj.interceptor
 * @author:LiBo/Alex
 * @create-date:2019-10-25 01:59
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@Slf4j
@Aspect
@Component
public class HttpInterceptor {

    /**
     * http模板功能
     */
    final String httpRequestTemplate = "【 http request 监控 => {}】";

    final String httpResponseTemplate = "【 http response 监控 => {}】";

    /**
     * 前置处理功能机制
     * @param joinPoint
     * @param httpAnchor
     */
    @Around("@annotation(httpAnchor)")
    public void prehandle(ProceedingJoinPoint joinPoint, HttpAnchor httpAnchor) {

        try {
            ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            if(servletRequestAttributes != null){
               HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
               httpServletRequest.setCharacterEncoding("UTF-8");
               log.info(httpRequestTemplate, HttpRequestData.builder().contextPath(httpServletRequest.getContextPath())
                       .method(httpServletRequest.getMethod())
                       .queryString(httpServletRequest.getQueryString())
                       .remoteUrl(httpServletRequest.getRemoteAddr())
                       .serverName(httpServletRequest.getServerName())
                       .requestUrl(httpServletRequest.getRequestURI())
                       .pathInfo(httpServletRequest.getPathInfo())
                       .params(httpServletRequest.getParameterMap().toString())
                       .cookies(Arrays.toString(httpServletRequest.getCookies()))
                       .build().toString());
            }
            joinPoint.proceed(joinPoint.getArgs());
           /* HttpServletResponse httpServletResponse = servletRequestAttributes.getResponse();
            log.info(httpResponseTemplate,HttpResponseData.builder().charset(httpServletResponse.getCharacterEncoding())
                    .contentType(httpServletResponse.getContentType()).build());*/
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


}
