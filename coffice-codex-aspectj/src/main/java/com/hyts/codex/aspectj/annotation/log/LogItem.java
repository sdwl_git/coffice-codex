package com.hyts.codex.aspectj.annotation.log;

import java.lang.annotation.*;

/**
 * @project-name:coffice-codex
 * @package-name:com.hyts.codex.aspectj.annotation.log
 * @author:LiBo/Alex
 * @create-date:2019-10-24 23:27
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@Target({ElementType.FIELD,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LogItem {

    /**
     * key值 对应参数的标识
     * @return
     */
    String key() default "undfined";

    /**
     * @return
     */
    boolean isEnabled() default true;

    /**
     * 是否是对象
     * @return
     */
    boolean isObject() default false;

}
