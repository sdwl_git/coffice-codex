/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.aspectj.annotation.http;

import lombok.Builder;
import lombok.Data;

import java.util.Collection;

/**
 * @project-name:coffice-codex
 * @package-name:com.hyts.codex.aspectj.annotation.http
 * @author:LiBo/Alex
 * @create-date:2019-10-25 11:48
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@Data
@Builder
public class HttpResponseData {

    private int status;

    private Collection<String> headerNames;

    private String contentType;

    private String charset;
}
