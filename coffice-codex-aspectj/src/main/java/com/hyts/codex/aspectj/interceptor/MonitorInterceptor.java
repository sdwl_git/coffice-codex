/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.aspectj.interceptor;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import com.alibaba.fastjson.JSONObject;
import com.hyts.codex.aspectj.annotation.run.MonitorScope;

import lombok.extern.slf4j.Slf4j;


/** 
 * @author LiBo/Alex
 * @see Aspect
 * @version V1.0 
 */
//@Component
@Aspect
@Slf4j
public class MonitorInterceptor {
    
    /**  
     * <b>监控执行:monitorExecFrame</b>
     * <p>统计方法执行时间以及返回输入参数及输出参数等</p>
     * @param joinPoint
     * @param monitor
     * @exception
     */ 
    @Around("@annotation(monitor)")
    public void monitorExecCount(ProceedingJoinPoint joinPoint,MonitorScope monitor) {
        if(monitor.isMetric()) {
            StopWatch stopWatch = new StopWatch();
            log.info("[coming in the before monitor scope,check the annotation is enabled:{}]",monitor.isMetric());
            stopWatch.start();
            try {
                log.info("[monitor param is {}]",JSONObject.toJSONString(joinPoint.getArgs()));
                Object result = joinPoint.proceed(joinPoint.getArgs());
                log.info("[monitor result is {}]",JSONObject.toJSONString(result));
            } catch (Throwable e) {
                log.warn("target invoke aop failure!",e);
            }
            stopWatch.stop();
            log.info("[the cost duration time is : {} s ]",stopWatch.getTotalTimeSeconds());
        }
    }
    
 }
