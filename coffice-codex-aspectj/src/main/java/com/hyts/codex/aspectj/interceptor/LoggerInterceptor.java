/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.aspectj.interceptor;

import com.hyts.codex.aspectj.annotation.log.LogItem;
import com.hyts.codex.aspectj.annotation.log.Logger;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @project-name:coffice-codex
 * @package-name:com.hyts.codex.aspectj.aop
 * @author:LiBo/Alex
 * @create-date:2019-10-24 11:50
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
//@Component
@Aspect
@Slf4j
public class LoggerInterceptor {


    final String logTemplate = "【 Logger监控 => 模块：{} - 层级：{} - 操作：{} - 描述：{} -  】";

    final String logKeyTemplate = "【 LogItem监控 => 參數：[ {}  ]  】";


    @Around("@annotation(logger)")
    public void prehandle(ProceedingJoinPoint joinPoint, Logger logger) {
        Class target = joinPoint.getTarget().getClass();
        Object[] args = joinPoint.getArgs();
        Object[] logKeys = new Object[args.length];
        for(int i= 0;i<args.length;i++){
            LogItem logItem = args[i].getClass().getAnnotation(LogItem.class);
            logKeys[i] = logItem;
        }
        log.info(logTemplate,logger.module(),logger.layer(),logger.operate(),logger.desc());
        log.info(logKeyTemplate, Arrays.toString(logKeys));
    }

}
