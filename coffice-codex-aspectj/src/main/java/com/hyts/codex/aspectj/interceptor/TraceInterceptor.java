/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.aspectj.interceptor;

import com.hyts.codex.aspectj.annotation.trace.TraceScope;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

/**
 * @project-name:coffice-codex
 * @package-name:com.hyts.codex.aspectj.aop
 * @author:LiBo/Alex
 * @create-date:2019-10-24 11:45
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
//@Component
@Aspect
@Slf4j
public class TraceInterceptor {

    /**
     * <b>trace-success</b>
     * @param joinPoint
     * @param trace
     * @exception
     */
    @AfterReturning("@annotation(trace)")
    public void traceSuccess(ProceedingJoinPoint joinPoint, TraceScope trace) {
        log.info("[coming in the before tracer scope,check the traceScope methodName:{}]",trace.methodName());
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        methodSignature.getDeclaringTypeName();
        methodSignature.getName();
        methodSignature.getParameterNames();
        methodSignature.getReturnType();
        //log.info("[coming in the after monitor scope,check the annotation is enabled:{}]",trace.isMetric());
        //Long end = Clock.systemDefaultZone().millis();

    }


    @AfterThrowing("@annotation(trace)")
    public void traceError(ProceedingJoinPoint joinPoint,TraceScope trace) {

        BindingResult bindingResult = null;
        for(Object arg:joinPoint.getArgs()){
            if(arg instanceof BindingResult){
                bindingResult = (BindingResult) arg;
            }
        }
        if(bindingResult != null){
            if(bindingResult.hasErrors()){
                String errorInfo="["+bindingResult.getFieldError().getField()+"]"+bindingResult.getFieldError().getDefaultMessage();
                return ;
            }
        }
    }

}
