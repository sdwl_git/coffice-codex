/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.aspectj.annotation.trace;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Documented
@Retention(RUNTIME)
@Target(METHOD)
/** 
 * @author LiBo/Alex
 * @version V1.0 
 * monitor the request or method to Tracescope create
 * trace info data to linking system
 */
public @interface TraceScope {
    
    /**
     * method name as trace - span info 
     * @return
     * @exception
     */
    public String methodName() default "";
    
    /**
     * param name as trace - span info 
     * @return
     * @exception
     */
    public String[] params() default "";
    
    /**
     * description
     * @return
     * @exception
     */
    public String description() default "";
    
}
