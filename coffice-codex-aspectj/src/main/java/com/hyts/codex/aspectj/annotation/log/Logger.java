/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.aspectj.annotation.log;

import com.hyts.codex.config.DataStatusEnum;
import com.hyts.codex.config.LogLevelEnum;
import com.hyts.codex.config.ModuleTypeEnum;

import java.lang.annotation.*;
import java.util.logging.Level;

/**
 * @project-name:coffice-codex
 * @package-name:com.hyts.codex.aspectj.annotation
 * @author:LiBo/Alex
 * @create-date:2019-10-24 11:48
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Logger {

    /**
     * 模块所在业务模块操作
      * @return
     */
    String module() default "";

    /**
     * 模块所在的层次操作-按照传统架构模式层次
     * @return
     */
    ModuleTypeEnum.LAYERErrorType layer() default ModuleTypeEnum.LAYERErrorType.DEFAULT;

    /**
     * 模块所在的服务操作-按照传统架构模式层次
     * @return
     */
    ModuleTypeEnum.OPRErrorType operate() default ModuleTypeEnum.OPRErrorType.DEFAULT;

    /**
     * 模块所在的功能 具体描述操作
     * @return
     */
    String desc() default  "";


    LogLevelEnum level() default LogLevelEnum.INFO;

}
