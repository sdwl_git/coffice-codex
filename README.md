### coffice-codex（服务标准化组件）

#### codex-demo
     codex-demo是使用codex的使用介绍，大家可以查看相关的使用方法。
#### coffice-codex前提介绍  
    coffice-codex作为我的一个标准化组件服务，没有追求过多的代码生成的功能以及速成开发步骤，目的主要是追求开发的标准化以及约定大于配置的思想，可以至今通过引入@Import*的注解，获取标准化服务功能，用户必用开发任何代码，只需要遵循标准，皆可以直接使用，不需要按照任何复杂和重复的工作，目前还在开发中，甚至连登录和注册都已经有了，内置的服务页面也有了，以后会更加提供更多和更稳定的功能。
    coffice-code主要包含三类组件，其中两种属于标准化服务组件（coffice-codex-*）、一种属于标准化应用组件（coffice-codex-app-*）。
#### coffice-codex标准化服务组件（服务级）
    - coffice-codex-author 认证服务组件-已经有了统一标准化的服务，可以热插拔，详情可以看demo
    - coffice-codex-shiro  shiro认证组件-已经有了统一标准化的服务，可以热插拔，详情可以看demo
    - coffice-codex-redis  权限认证组件-已经有了统一标准化的服务，可以热插拔，详情可以看demo
#### coffice-codex标准化应用组件（应用级）
    - coffice-codex-app-sign 登录、注册、访问、权限一体化处理的标准应用，可以热插拔，详情可以看demo
#### coffice-codex标准化服务组件（定义级）
    其余都属于定义级别，定义结构，较为灵活，希望可以供开发者参考，如有纰漏多多指正。