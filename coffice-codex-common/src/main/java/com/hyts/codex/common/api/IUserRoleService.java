package com.hyts.codex.common.api;

import com.hyts.codex.base.dto.ResultDTO;
import com.hyts.codex.base.vo.ResultVO;
import com.hyts.codex.user.bo.UserRoleBO;

import java.util.List;

/**
 * @project-name:coffice-codex
 * @package-name:com.hyts.codex.common.api
 * @author:Libo/Alex
 * @create-date:2019-04-27 20:12
 * @copyright:libo-hyts-github
 * @email:libo2dev@aliyun.com
 * @description:此类主要用于: 用户和角色表的相关查询动作
 */
public interface IUserRoleService {


    /**
     * 查找角色数据通过userId用户id数据信息列表
     * @return
     */
    ResultDTO<ResultVO<UserRoleBO>> findRoleDataByUserId(Long userId);

    /**
     * 查找角色数据通过RoleId角色id数据信息列表
     * @return
     */
    ResultDTO<ResultVO<UserRoleBO>> findRoleDataByRoleId(Long roleId);

    /**
     * 查找橘色数据通过RoleCode角色编码查找相关数据信息列表
     * @return
     */
    ResultDTO<ResultVO<UserRoleBO>> findRoleDataByRoleCode(String roleCode);

}
