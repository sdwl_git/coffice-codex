package com.hyts.codex.common.api;

import com.hyts.codex.base.dto.ResultDTO;
import com.hyts.codex.base.vo.ResultVO;
import com.hyts.codex.user.bo.MenuBO;

import java.util.List;

/**
 * project-name:coffice-codex
 * package-name:com.hyts.codex.common.api
 * author:Libo/Alex
 * create-date:2019-04-28 21:07
 * copyright:libo-hyts-github
 * email:libo2dev@aliyun.com
 * description:此类主要用于:菜单信息的业务逻辑接口
 */
public interface IMenuService {

    ResultDTO<ResultVO<MenuBO>> findMenuByRoleIds(List<Long> roleIdList);

}
