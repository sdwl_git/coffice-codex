/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.common.api;

import com.hyts.codex.base.dto.ResultDTO;
import com.hyts.codex.base.vo.ResultVO;
import com.hyts.codex.user.bo.UserBO;
import com.hyts.codex.user.dto.LoginDTO;

/** 
 * @author LiBo/Alex
 * @since TODO引入特定改变发布版本
 * @version V1.0 
 */
public interface IUserService {

    
    ResultDTO<ResultVO<UserBO>> login(LoginDTO loginDTO);
    
    ResultDTO<ResultVO<UserBO>> register(UserBO bo);

    ResultDTO<ResultVO<UserBO>> loadUserIdByUserName(String userName);

}
