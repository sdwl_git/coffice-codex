/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts;

import com.alibaba.fastjson.JSONObject;
import com.hyts.test.bean.TestValidatorBean;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * @project-name:coffice-codex
 * @package-name:com.hyts
 * @author:LiBo/Alex
 * @create-date:2019-10-26 00:20
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
/*@SpringBootTest(classes = DemoApplication.class)
@RunWith(SpringRunner.class)
@WebAppConfiguration*/
public class TestController {


    //@Autowired
    public WebApplicationContext webApplicationContext;

    //@Autowired
    //ValidateConfiguration validateConfiguration;

    public MockMvc mvc;


    @Before
    public void initBefore(){
        //mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }


    @Test
    public void testGet(){

        try {
            MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/index?name=")).andReturn();
            System.out.println(mvcResult.getResponse().getErrorMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testPost(){
        /*try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name","");
            MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/access/test").
                    contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonObject.toJSONString())).andReturn();
            System.out.println(mvcResult.getResponse().getErrorMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        TestRestTemplate testRestTemplate = new TestRestTemplate();
        System.out.println(testRestTemplate.postForObject("http://localhost:8082/access/test",JSONObject.toJSON(new TestValidatorBean()),Object.class));

    }


    @Test
    public void testValidate(){

        //System.out.println(validateConfiguration.getMappings());
        /*try {
            MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/index?name=")).andReturn();
            System.out.println(mvcResult.getResponse().getErrorMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }*/

    }



}
