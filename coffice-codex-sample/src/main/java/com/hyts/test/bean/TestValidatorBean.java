/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.test.bean;


import com.hyts.codex.validator.ValidateInterface;
import com.hyts.codex.validator.ValidatePattern;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * @project-name:coffice-codex
 * @package-name:com.hyts.test.bean
 * @author:LiBo/Alex
 * @create-date:2019-10-26 00:17
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
public class TestValidatorBean {

    @Pattern(regexp = "sdasda",message = "12312")
    @NotEmpty(message = "{string.notEmpty}")
    @Size(min = 30)
    private String name;

}
