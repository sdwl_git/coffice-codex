/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.test.boot;

import com.hyts.codex.app.sign.config.ImportAppSign;
import com.hyts.laucher.boot.ServerBootLaucher;
import com.hyts.laucher.boot.servlet.impl.TomcatWebServerServletLaucher;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @project-name:coffice-codex
 * @package-name:com.hyts.test
 * @author:LiBo/Alex
 * @create-date:2019-10-17 16:13
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
//TODO 暂时不支持web view视图的支持

//@SpringBootApplication
public class DemoBootApplication extends TomcatWebServerServletLaucher {

    @Override
    public int order() {
        return 0;
    }

    @Override
    public int port() {
        return 8089;
    }

    @Override
    public String contextPath() {
        return "/web";
    }

}
