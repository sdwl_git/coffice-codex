/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.test.web;

import com.hyts.codex.aspectj.annotation.http.HttpAnchor;
import com.hyts.codex.aspectj.annotation.log.LogItem;
import com.hyts.codex.aspectj.annotation.log.Logger;
import com.hyts.codex.aspectj.annotation.run.MonitorScope;
import com.hyts.codex.aspectj.annotation.validate.ValidateScope;
import com.hyts.test.bean.TestValidatorBean;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.IOException;
import java.text.MessageFormat;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 */
@Controller
public class DemoController {
    

    @RequestMapping("/access/toLogin/{index}")
    public String login(HttpServletRequest request,HttpServletResponse response,@PathVariable
            @LogItem(key = "index") int index) {
        return MessageFormat.format("redirect:/stateless/{0}/login", index);
    }


    @RequestMapping("/index")
    public void index(HttpServletRequest request, HttpServletResponse response,  @Valid @RequestBody TestValidatorBean testValidatorBean) {
        try {
            request.getRequestDispatcher("index.html").forward(request, response);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("/access/index2")
    public String index2(HttpServletRequest request,HttpServletResponse response) {
       /* try {
            request.getRequestDispatcher("login.html").forward(request, response);
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }*/
       return  "login";
    }

    @ValidateScope
    @ResponseBody
    @RequestMapping("/access/test")
    public String index2(@Valid TestValidatorBean testValidatorBean) {
        return  "success";
    }

}
