/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.rabbitmq.config;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

import com.hyts.codex.rabbitmq.domain.ExchangeBean;
import com.hyts.codex.rabbitmq.domain.QueueBean;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 */
//@ConditionalOnClass({CachingConnectionFactory.class,RabbitTemplate.class,RabbitListener.class})
//@ConditionalOnMissingBean({ConnectionFactory.class,RabbitTemplate.class,RabbitListener.class})
//@Configuration
//@EnableConfigurationProperties({QueueBean.class,ExchangeBean.class})
public class RabbitMQConfiguration {
        
    /**
     * 环境变量
     */
    @Autowired
    Environment env;

    /**
     * queue
     * @return
     * @exception
     */
    @Bean
    public Queue defaultQueue(QueueBean queueBean) {
        return new Queue(queueBean.getName(),queueBean.isDuration(),queueBean.isExclusive(),queueBean.isAutoDelete());
    }
        
    /**
     * topic 
     * @return
     * @exception
     */
    @Bean
    public TopicExchange defaultTopicExchange(ExchangeBean exchangeBean) {
        return new TopicExchange(exchangeBean.getName(),exchangeBean.isDuration(),exchangeBean.isAutoDelete(),exchangeBean.getArguments());
    }
    
    
    /**
     * RabbitMQ template tool class
     * @param connectionFactory
     * @return
     * @exception
     */
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        return new RabbitTemplate(connectionFactory);
    }
    
    
    /**
     * connectionFactory
     * @return
     * @exception
     */
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory(env.getProperty(ParamConfiguration.RABBITMQ_QUEUE_BASE_PREFIX+"ip"),Integer.parseInt(env.getProperty(ParamConfiguration.RABBITMQ_QUEUE_BASE_PREFIX+"port")));
        connectionFactory.setUsername(env.getProperty(ParamConfiguration.RABBITMQ_QUEUE_BASE_PREFIX+"username"));
        connectionFactory.setPassword(env.getProperty(ParamConfiguration.RABBITMQ_QUEUE_BASE_PREFIX+"password"));
        return connectionFactory;
    }
    
    
    @Bean
    public Binding bindingApple(Queue queue, TopicExchange topicExchange) {
        return BindingBuilder.bind(queue).to(topicExchange).with("");
    }
    
}
