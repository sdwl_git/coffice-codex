package com.hyts.codex.scanner.annotation;

import java.lang.annotation.*;

/**
 * Created by alex on 2019-05-18.
 * Package:com.hyts.codex.scanner.annotation
 */
@Documented
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ScannerScope {
}
