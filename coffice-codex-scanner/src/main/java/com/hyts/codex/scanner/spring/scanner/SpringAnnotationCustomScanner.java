package com.hyts.codex.scanner.spring.scanner;

import com.hyts.codex.scanner.spring.filter.AnnotationCustomeTypeFilter;
import lombok.*;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.util.CollectionUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * project-name:coffice-codex
 * package-name:com.hyts.codex.scanner.spring.scanner
 * author:Libo/Alex
 * @see ClassPathBeanDefinitionScanner
 * create-date:2019-05-18 18:41
 * copyright:libo-hyts-github
 * email:libo2dev@aliyun.com
 * description:此类主要用于: spring服务框架的自定义定制化注解扫描器，采用扫描方式进行控制注解扫描机制
 */
@Accessors(chain=true)
//@Builder
public class SpringAnnotationCustomScanner extends ClassPathBeanDefinitionScanner {


    /**
     * annotationCustomTypeFilter-过滤器服务
     */
    private List<AnnotationCustomeTypeFilter> annotationCustomeTypeFilters;

    /**
     * addTypeFilter工具
     * @param annotationCustomeTypeFilter
     * @return
     */
    public SpringAnnotationCustomScanner addTypeFilter(AnnotationCustomeTypeFilter annotationCustomeTypeFilter){
        if(CollectionUtils.isEmpty(annotationCustomeTypeFilters)){
            annotationCustomeTypeFilters = new LinkedList<>();
        }
        annotationCustomeTypeFilters.add(annotationCustomeTypeFilter);
        addIncludeFilter(annotationCustomeTypeFilter);
        return this;
    }

    /**
     * addTypeFilter工具
     * @param annotationCustomeTypeFilter
     * @return
     */
    public SpringAnnotationCustomScanner addTypeFilters(List<AnnotationCustomeTypeFilter> annotationCustomeTypeFilter){
        if(CollectionUtils.isEmpty(annotationCustomeTypeFilters)){
            annotationCustomeTypeFilters = new LinkedList<>();
        }
        annotationCustomeTypeFilters.addAll(annotationCustomeTypeFilter);
        annotationCustomeTypeFilter.parallelStream().forEach(param->{
            addIncludeFilter(param);
        });
        return this;
    }

    @Override
    public int scan(String... basePackages) {
        return super.scan(basePackages);
    }

    @Override
    protected Set<BeanDefinitionHolder> doScan(String... basePackages) {
        return super.doScan(basePackages);
    }

    @Override
    protected void postProcessBeanDefinition(AbstractBeanDefinition beanDefinition, String beanName) {
        super.postProcessBeanDefinition(beanDefinition, beanName);
    }

    @Override
    protected void registerBeanDefinition(BeanDefinitionHolder definitionHolder, BeanDefinitionRegistry registry) {
        super.registerBeanDefinition(definitionHolder, registry);
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        super.setResourceLoader(resourceLoader);
    }

    @Override
    public void setResourcePattern(String resourcePattern) {
        super.setResourcePattern(resourcePattern);
    }

    @Override
    public void addIncludeFilter(TypeFilter includeFilter) {
        super.addIncludeFilter(includeFilter);
    }

    @Override
    public void addExcludeFilter(TypeFilter excludeFilter) {
        super.addExcludeFilter(excludeFilter);
    }


    public SpringAnnotationCustomScanner(BeanDefinitionRegistry registry, List<AnnotationCustomeTypeFilter> annotationCustomeTypeFilters){
        super(registry);
        this.annotationCustomeTypeFilters = annotationCustomeTypeFilters;
        addTypeFilters(annotationCustomeTypeFilters);
    }

    /**
     * @param registry
     * @param annotationCustomeTypeFilter
     */
    public SpringAnnotationCustomScanner(BeanDefinitionRegistry registry, AnnotationCustomeTypeFilter annotationCustomeTypeFilter) {
        super(registry);
        addTypeFilter(annotationCustomeTypeFilter);
    }

    /*************************************SpringAnnotationCustomFilter 过滤器服务功能*******************************************************************/

    /**
     * @param registry
     * @param useDefaultFilters
     * @param annotationCustomeTypeFilter
     */
    /*SpringAnnotationCustomScanner(BeanDefinitionRegistry registry, boolean useDefaultFilters, AnnotationCustomeTypeFilter annotationCustomeTypeFilter) {
        super(registry, useDefaultFilters);
        addTypeFilter(annotationCustomeTypeFilter);
    }*/

    /**
     * @param registry
     * @param useDefaultFilters
     * @param environment
     * @param annotationCustomeTypeFilter
     */
    /*SpringAnnotationCustomScanner(BeanDefinitionRegistry registry, boolean useDefaultFilters, Environment environment, AnnotationCustomeTypeFilter annotationCustomeTypeFilter) {
        super(registry, useDefaultFilters, environment);
        addTypeFilter(annotationCustomeTypeFilter);
    }*/
}
