package com.hyts.codex.scanner.spring.filter;

import org.springframework.core.type.filter.AnnotationTypeFilter;

import java.lang.annotation.Annotation;

/**
 * project-name:coffice-codex
 * package-name:com.hyts.codex.scanner.spring.filter
 * author:Libo/Alex
 * create-date:2019-05-18 18:24
 * copyright:libo-hyts-github
 * email:libo2dev@aliyun.com
 * description:此类主要用于: 自定义总体注解注册服务控制器-进入spring生态环境
 */
public class AnnotationCustomeTypeFilter extends AnnotationTypeFilter {


    public AnnotationCustomeTypeFilter(Class<? extends Annotation> annotationType) {
        super(annotationType);
    }

    public AnnotationCustomeTypeFilter(Class<? extends Annotation> annotationType, boolean considerMetaAnnotations) {
        super(annotationType, considerMetaAnnotations);
    }

    public AnnotationCustomeTypeFilter(Class<? extends Annotation> annotationType, boolean considerMetaAnnotations, boolean considerInterfaces) {
        super(annotationType, considerMetaAnnotations, considerInterfaces);
    }
}
