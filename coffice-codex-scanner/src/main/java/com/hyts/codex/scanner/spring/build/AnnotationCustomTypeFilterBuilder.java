package com.hyts.codex.scanner.spring.build;

import com.hyts.codex.scanner.spring.filter.AnnotationCustomeTypeFilter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.lang.annotation.Annotation;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

/**
 * project-name:coffice-codex
 * package-name:com.hyts.codex.scanner.spring.build
 * author:Libo/Alex
 * create-date:2019-05-18 18:37
 * copyright:libo-hyts-github
 * email:libo2dev@aliyun.com
 * description:此类主要用于: annoationTypeFilter 构建器
 */

@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AnnotationCustomTypeFilterBuilder {


    private boolean containInterface;


    private boolean containMetaAnnotation;


    private String[] annotations;

    /**
     * @return
     */
    public List<AnnotationCustomeTypeFilter> buildCustomTypeFilters(){
        //注解控制判断
        List<AnnotationCustomeTypeFilter> annotationCustomeTypeFilters = new LinkedList<>();
        if(annotations == null || annotations.length == 0){
            return annotationCustomeTypeFilters;
        }
        try {
            for(int i = 0 ; i < annotations.length ; i++){
                annotationCustomeTypeFilters.add(new AnnotationCustomeTypeFilter((Class<? extends Annotation>) Class.forName(annotations[i])));
            }
        }catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return annotationCustomeTypeFilters;
    }

}
