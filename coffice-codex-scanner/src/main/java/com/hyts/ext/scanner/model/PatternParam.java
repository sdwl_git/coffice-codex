/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.ext.scanner.model;

import com.hyts.ext.scanner.pattern.PatternBeanFactory;

/** 
 * @author LiBo/Alex
 * @see TODO制定另外一个主体的链接
 * @since TODO引入特定改变发布版本
 * @version V1.0 
 */
public class PatternParam extends PatternBeanFactory {

    /**  
     * @constructor：PatternParam  
     * @param resourcePattern  
     */ 
    public PatternParam(String resourcePattern) {
	super(resourcePattern);
    }
    
    public PatternParam() {
	super();
    }
}
