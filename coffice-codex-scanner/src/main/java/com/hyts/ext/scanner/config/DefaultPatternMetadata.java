/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.ext.scanner.config;

import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

/** 
 * @author LiBo/Alex
 * @since v1.0
 * @version V1.0 
 */
public final class DefaultPatternMetadata {
	
	/**  
	 * <p>路径解析器操作-采用spring核心的PathMatchingResourcePatternResolver</p> 
	 *   此类为ApplicationContext以及BeanFactory扫描springbean组件的基础
	 * @fieldName DEFAULT_PATH_PATTERN_RESOLVER
	 * @fieldType ResourcePatternResolver
	 */ 
	public static final ResourcePatternResolver DEFAULT_PATH_PATTERN_RESOLVER = new PathMatchingResourcePatternResolver();
	
	/**  
	 * <p>路径资源模式按照spring框架规范的解析和设置</p> 
	 * @fieldName DEFAULT_PATH_RESOURCE_PATTERN
	 * @fieldType String
	 */ 
	public static final String DEFAULT_PATH_RESOURCE_PATTERN = "/**/*.class";
	
	
}
