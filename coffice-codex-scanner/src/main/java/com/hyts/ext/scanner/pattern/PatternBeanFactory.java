/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.ext.scanner.pattern;

import java.util.LinkedList;
import java.util.List;

import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.filter.TypeFilter;

import com.hyts.ext.scanner.config.DefaultPatternMetadata;


/** 
 * @author LiBo/Alex
 * @version V1.0 
 */
public class PatternBeanFactory extends PatternMetadata{
	
	/**  
	 * <p>路径分析器-spring框架的路径扫描的基础接口</p> 
	 * @fieldName pathPatternResolver
	 * @fieldType ResourcePatternResolver
	 */ 
	private ResourcePatternResolver pathPatternResolver;

	/**  
	 * <p>Spring 框架的类型处理过滤器</p> 
	 * @fieldName typeFilters
	 * @fieldType List<TypeFilter>
	 */ 
	private List<TypeFilter> typeFilters;
	
	/**  
	 * @name pathPatternResolver's getter method
	 * @param none
	 * @return pathPatternResolver
	 */
	public ResourcePatternResolver getPathPatternResolver() {
		return pathPatternResolver;
	}
	
	/**  
	 * @name pathPatternResolver's setter method
	 * @param pathPatternResolver
	 * @return void
	 */
	public void setPathPatternResolver(ResourcePatternResolver pathPatternResolver) {
		this.pathPatternResolver = pathPatternResolver;
	}

	/**  
	 * @name typeFilters's getter method
	 * @param none
	 * @return typeFilters
	 */
	public List<TypeFilter> getTypeFilters() {
		return typeFilters;
	}

	/**  
	 * @name typeFilters's setter method
	 * @param typeFilters
	 * @return void
	 */
	public void setTypeFilters(List<TypeFilter> typeFilters) {
		this.typeFilters = typeFilters;
	}

	/**  
	 * @constructor：PatternBeanFactory  
	 * @param resourcePattern
	 * @param pathPatternResolver  
	 */ 
	public PatternBeanFactory(String resourcePattern, ResourcePatternResolver pathPatternResolver) {
		super(resourcePattern);
		this.pathPatternResolver = pathPatternResolver;
		this.typeFilters = new LinkedList<>();
	}

	/**  
	 * @constructor：PatternBeanFactory  
	 * @param resourcePattern
	 * @param typeFilters  
	 */ 
	public PatternBeanFactory(String resourcePattern, List<TypeFilter> typeFilters) {
		super(resourcePattern);
		this.pathPatternResolver = DefaultPatternMetadata.DEFAULT_PATH_PATTERN_RESOLVER;
		this.typeFilters = typeFilters;
	}

	/**  
	 * @constructor：PatternBeanFactory  
	 * @param resourcePattern
	 * @param pathPatternResolver
	 * @param typeFilters  
	 */ 
	public PatternBeanFactory(String resourcePattern, ResourcePatternResolver pathPatternResolver,
			List<TypeFilter> typeFilters) {
		super(resourcePattern);
		this.pathPatternResolver = pathPatternResolver;
		this.typeFilters = typeFilters;
	}
	
	/**  
	 * @constructor：PatternBeanFactory  
	 * @param resourcePattern  
	 */ 
	public PatternBeanFactory(String resourcePattern) {
	    super(resourcePattern);
	    this.pathPatternResolver = DefaultPatternMetadata.DEFAULT_PATH_PATTERN_RESOLVER;
	    this.typeFilters = new LinkedList<>();
	}

	/**  
	 * @constructor：PatternBeanFactory  
	 * @param resourcePattern  
	 */ 
	public PatternBeanFactory() {
	    super(DefaultPatternMetadata.DEFAULT_PATH_RESOURCE_PATTERN);
	    this.pathPatternResolver = DefaultPatternMetadata.DEFAULT_PATH_PATTERN_RESOLVER;
	    this.typeFilters = new LinkedList<>();
	}
}
