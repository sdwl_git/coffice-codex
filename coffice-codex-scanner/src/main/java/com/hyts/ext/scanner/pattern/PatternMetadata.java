/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.ext.scanner.pattern;

/** 
 * @author LiBo/Alex
 * @see org.springframework.core.io.support.ResourcePatternResolver
 * @since  V1.0 
 * @version V1.0 
 */
public abstract class PatternMetadata {
	
	/**  
	 * <p>资源匹配模式</p> 
	 * @fieldName resourcePattern
	 * @fieldType String
	 */ 
	private String resourcePattern;

	/**  
	 * @name resourcePattern's getter method
	 * @param none
	 * @return resourcePattern
	 */
	public String getResourcePattern() {
		return resourcePattern;
	}

	/**  
	 * @name resourcePattern's setter method
	 * @param resourcePattern
	 * @return void
	 */
	public void setResourcePattern(String resourcePattern) {
		this.resourcePattern = resourcePattern;
	}

	/**  
	 * @constructor：PatternMetadata  
	 * @param resourcePattern  
	 */ 
	public PatternMetadata(String resourcePattern) {
		super();
		this.resourcePattern = resourcePattern;
	}

	/**  
	 * @constructor：PatternMetadata    
	 */ 
	public PatternMetadata() {
		super();
	}
	
}
