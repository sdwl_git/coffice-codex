/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.ext.scanner.bean.base;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 * scanner create or build 's base class or super class 
 * define action or operation
 * support the dev or consumer to extends yourself scan metadata
 */
public abstract class ParamFactoryBean<T,V> {
	
	/**  
	 * <p>扫描包</p> 
	 * @fieldName scanPackages
	 * @fieldType T
	 */ 
	T scanPackages;
	
	/**  
	 * <p>扫描注解</p> 
	 * @fieldName scanAnnotations
	 * @fieldType V
	 */ 
	V scanAnnotations;
	
	/**  
	 * <b>build服务对象</b>
	 * @param scanPackages
	 * @param scanAnnotations
	 * @exception
	 */ 
	public abstract <R> ParamFactoryBean<T,V> build(T scanPackage,V scanAnnotation,Class<R> resultType);

	/**  
	 * @name scanPackages's getter method
	 * @param none
	 * @return scanPackages
	 */
	public T getScanPackages() {
		return scanPackages;
	}

	/**  
	 * @name scanPackages's setter method
	 * @param scanPackages
	 * @return void
	 */
	public void setScanPackages(T scanPackages) {
		this.scanPackages = scanPackages;
	}

	/**  
	 * @name scanAnnotations's getter method
	 * @param none
	 * @return scanAnnotations
	 */
	public V getScanAnnotations() {
		return scanAnnotations;
	}

	/**  
	 * @name scanAnnotations's setter method
	 * @param scanAnnotations
	 * @return void
	 */
	public void setScanAnnotations(V scanAnnotations) {
		this.scanAnnotations = scanAnnotations;
	}
	
	
	
	
}
