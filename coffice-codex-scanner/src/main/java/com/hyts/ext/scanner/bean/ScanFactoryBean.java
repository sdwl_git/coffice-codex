/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.ext.scanner.bean;

import org.springframework.stereotype.Component;

import com.hyts.ext.scanner.model.PatternParam;
import com.hyts.ext.scanner.model.ScannerParam;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 */
public class ScanFactoryBean {
    
    /**  
     * <p>扫描参数</p> 
     * @fieldName scannerParam
     * @fieldType ScannerParam
     */ 
    private ScannerParam scannerParam;
    
    /**  
     * <p>匹配模式参数</p> 
     * @fieldName patternParam
     * @fieldType PatternParam
     */ 
    private PatternParam patternParam;

    /**  
     * @constructor：ScannerFactoryBean  
     * @param scannerParam
     * @param patternParam  
     */ 
    public ScanFactoryBean(ScannerParam scannerParam, PatternParam patternParam) {
	super();
	this.scannerParam = scannerParam;
	this.patternParam = patternParam;
    }
    
    /**  
     * @constructor：ScannerFactoryBean  
     * @param scannerParam  
     */ 
    public ScanFactoryBean(ScannerParam scannerParam) {
	super();
	this.scannerParam = scannerParam;
	this.patternParam = new PatternParam();
    }
    
    /**  
     * @constructor：ScanFactoryBean    
     */ 
    public ScanFactoryBean() {
	super();
    }

    /**  
     * @name scannerParam's getter method
     * @param none
     * @return scannerParam
     */
    public ScannerParam getScannerParam() {
        return scannerParam;
    }

    /**  
     * @name scannerParam's setter method
     * @param scannerParam
     * @return void
     */
    public void setScannerParam(ScannerParam scannerParam) {
        this.scannerParam = scannerParam;
    }

    /**  
     * @name patternParam's getter method
     * @param none
     * @return patternParam
     */
    public PatternParam getPatternParam() {
        return patternParam;
    }

    /**  
     * @name patternParam's setter method
     * @param patternParam
     * @return void
     */
    public void setPatternParam(PatternParam patternParam) {
        this.patternParam = patternParam;
    }
}
