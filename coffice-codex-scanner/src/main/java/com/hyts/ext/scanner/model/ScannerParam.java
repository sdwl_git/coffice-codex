/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.ext.scanner.model;
import java.util.ArrayList;
import java.util.Collection;
import com.hyts.ext.scanner.bean.ScanMetadata;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 */
public final class ScannerParam extends ScanMetadata {
	
	
	public ScannerParam(Collection<String> scanPackages,Collection<Class<?>>scanAnnotations) {
		super.setScanPackages(scanPackages);
		super.setScanAnnotations(scanAnnotations);
	}
	
	public ScannerParam includeAnnotation(boolean includeAnnotations) {
		this.setIncludeAnnotationScope(includeAnnotations);
		return this;
	}
	
	public ScannerParam includePackage(boolean includeAnnotations) {
		this.setIncludeAnnotationScope(includeAnnotations);
		return this;
	}
	
}
