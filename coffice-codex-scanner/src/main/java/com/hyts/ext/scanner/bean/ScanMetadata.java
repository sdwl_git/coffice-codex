/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.ext.scanner.bean;

import java.util.ArrayList;
import java.util.Collection;

import com.hyts.ext.scanner.bean.base.ParamFactoryBean;
import com.hyts.ext.scanner.bean.base.ResultFactoryBean;
import com.hyts.ext.scanner.config.DefaultScanMetadata;

/** 
 * @author LiBo/Alex
 * @see 
 * @since V1.0
 * @version V1.0 
 * scan definition bean to defined the scan properties
 * all the operation and function is base on the spring scan and resource framework
 * attribute:
 * 1.scan scope 
 * 2.scan annotation
 * 3.scan class include annotation ?
 * 4.scan class include interface ?
 */
public abstract class ScanMetadata extends ParamFactoryBean<Collection<String>,Collection<Class<?>>>{
	
	/**  
	 * <p>是否包含注解类的扫描 默认为不扫描</p> 
	 * @fieldName includeAnnotationScope
	 * @fieldType boolean
	 */ 
	private boolean includeAnnotationScope;
	
	/**  
	 * <p>是否包含接口类的扫描  默认为不扫描</p> 
	 * @fieldName includeInterfaceScope
	 * @fieldType boolean
	 */ 
	private boolean includeInterfaceScope;
	
	/* 
	 * @param scanPackage
	 * @param scanAnnotation
	 * @param resultType
	 * @return
	 * @see com.hyts.ext.scanner.bean.base.ParamFactoryBean#build(java.lang.Object, java.lang.Object, java.lang.Class)  
	 * @exception
	 */ 
	@Override
	public <R> ParamFactoryBean<Collection<String>,Collection<Class<?>>> build(Collection<String> scanPackage, Collection<Class<?>> scanAnnotation,
			Class<R> resultType) {
		includeAnnotationScope = DefaultScanMetadata.DEFAULT_SCAN_ANNOTATION;
		includeInterfaceScope = DefaultScanMetadata.DEFAULT_SCAN_INTERFACE;
		return build(scanPackage,scanAnnotation,resultType,includeAnnotationScope,includeInterfaceScope);
	}
	
	/**  
	 * @param scanPackage
	 * @param scanAnnotation
	 * @param resultType
	 * @param includeAnnotationScope
	 * @param includeInterfaceScope
	 * @return
	 * @exception
	 */ 
	public <R> ParamFactoryBean<Collection<String>,Collection<Class<?>>> build(Collection<String> scanPackage, Collection<Class<?>> scanAnnotation,
			Class<R> resultType,boolean includeAnnotationScope,boolean includeInterfaceScope) {
		this.setScanAnnotations(scanAnnotation);
		this.setScanPackages(scanPackage);
		return this;
	}
	/**  
	 * @param scanPackage
	 * @param scanAnnotation
	 * @param resultType
	 * @param includeAnnotationScope
	 * @return
	 * @exception
	 */ 
	public <R> ParamFactoryBean<Collection<String>,Collection<Class<?>>> build(Collection<String> scanPackage, Collection<Class<?>> scanAnnotation,
			Class<R> resultType,boolean includeAnnotationScope) {
		includeInterfaceScope = DefaultScanMetadata.DEFAULT_SCAN_INTERFACE;
		return build(scanPackage,scanAnnotation,resultType,includeAnnotationScope,includeInterfaceScope);
	}
	
	/**  
	 * @constructor：ScanMetadata    
	 */ 
	public ScanMetadata() {
		super();
	}
	
	/**  
	 * @name includeAnnotationScope's getter method
	 * @param none
	 * @return includeAnnotationScope
	 */
	public boolean isIncludeAnnotationScope() {
		return includeAnnotationScope;
	}

	/**  
	 * @name includeAnnotationScope's setter method
	 * @param includeAnnotationScope
	 * @return void
	 */
	public void setIncludeAnnotationScope(boolean includeAnnotationScope) {
		this.includeAnnotationScope = includeAnnotationScope;
	}

	/**  
	 * @name includeInterfaceScope's getter method
	 * @param none
	 * @return includeInterfaceScope
	 */
	public boolean isIncludeInterfaceScope() {
		return includeInterfaceScope;
	}

	/**  
	 * @name includeInterfaceScope's setter method
	 * @param includeInterfaceScope
	 * @return void
	 */
	public void setIncludeInterfaceScope(boolean includeInterfaceScope) {
		this.includeInterfaceScope = includeInterfaceScope;
	}
	
	
}
