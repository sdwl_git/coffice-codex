/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.base;

import com.hyts.codex.base.bo.BaseBO;
import com.hyts.codex.base.dto.ResultDTO;
import com.hyts.codex.base.vo.ResultVO;
import com.hyts.codex.config.ResultTypeEnum;

import com.hyts.codex.user.bo.UserRoleBO;
import lombok.Getter;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;
import java.util.List;

/** 
 * @author LiBo/Alex
 * @see
 * @since V1.0
 * @version V1.0 
 */
public final class DefaultBeanHandler/*<T extends BaseBO<Long>>*/ {
        
    
    @Getter
    private static final DefaultBeanHandler INSTANCE = new DefaultBeanHandler();
    
    /**  
     * @param data
     * @return
     * @exception
     * @description 根据传入的对象转换一般为BO-转换为VO对象
     */ 
    public <T extends BaseBO<Long>> ResultVO<T> newResultVO(T data){
        ResultVO<T> vo = new ResultVO<>();
        vo.setData(data);
        return vo;
    }

    /**
     * @param data
     * @return
     * @exception
     * @description 根据传入的对象转换一般为BO-转换为VO集合对象
     */
    public <T extends BaseBO<Long>> ResultVO<T> newResultVO(List<T> data){
        ResultVO<T> vo = new ResultVO<>();
        vo.setDataList(data);
        return vo;
    }
    
    /**  
     * @return
     * @exception
     * @description 生成一个新的空VO对象
     */ 
    public <T extends BaseBO<Long>> ResultVO<T> newResultVO(){
        ResultVO<T> vo = new ResultVO<>();
        return vo;
    }

    /**
     * @return
     * @exception
     * @description 生成一个新的空DTO对象-指定对于的泛型操作类
     */
    public <R,T extends ResultVO<R>> ResultDTO<T> newResultDTO() {
        return new ResultDTO<T>();
    }

    /**  
     * @return
     * @exception
     * @description 生成一个新的空DTO对象-指定对于的泛型操作类
     */ 
    public <R,T extends ResultVO<R>> ResultDTO<T> newResultDTO(Class<R> clazz) {
        return new ResultDTO<T>();
    }
    
    /**  
     * @param data
     * @return
     * @exception
     * @description 生成一个新的空DTO对象-指定对于的泛型操作类对象
     */ 
    public <R ,T extends ResultVO<R>> ResultDTO<T> newResultDTO(R data) {
        return new ResultDTO<T>();
    }


    /**
     * @param data
     * @return
     * @exception
     */ 
    public <W extends BaseBO<Long>,R,T extends ResultVO<R>> ResultDTO<ResultVO<W>> returnSuccess(W data) {
        ResultVO<W> vo =  newResultVO(data);
        return newResultDTO(data).success(vo);
    }

    /**
     * @param data
     * @return
     * @exception
     */
    public <W extends BaseBO<Long>,R,T extends ResultVO<R>> ResultDTO<ResultVO<W>> returnListSuccess(List<W> data,Class<W> clazz) {
        return newResultDTO(clazz).success().setResult(newResultVO(data));
    }
    
    /**  
     * @param clazz
     * @return
     * @exception
     */ 
    public <W extends BaseBO<Long>,R,T extends ResultVO<R>> ResultDTO<ResultVO<W>> returnSuccess(Class<W> clazz) {
        return newResultDTO(clazz).success();
    }
    
    
    /**  
     * @param
     * @return
     * @exception
     */ 
    public <W extends BaseBO<Long>,R,T extends ResultVO<R>> ResultDTO<ResultVO<W>> returnFailure(Class<W> clazz,ResultTypeEnum type) {
        return newResultDTO(clazz).failure(type);
    }
    
    /**  
     * @param clazz
     * @return
     * @exception
     */ 
    public <W extends BaseBO<Long>,R,T extends ResultVO<R>> ResultDTO<ResultVO<W>> returnFailure(Class<W> clazz) {
        return newResultDTO(clazz).failure();
    }

    /**
     * @param
     * @return
     * @exception
     */
    /*public <W extends BaseBO<Long>,R,T extends ResultVO<R>> ResultDTO<ResultVO<W>> returnListFailure(ResultTypeEnum type) {
*//*
        List<W> userRolePOS = Collections.emptyList();
*//*
        return newResultDTO().failure(type);
    }*/

    /**
     * @param
     * @return
     * @exception
     */
    /*public <W extends BaseBO<Long>,R,T extends ResultVO<R>> ResultDTO<ResultVO<W>> returnListFailure(Class<W> clazz) {
        List<W> userRolePOS = Collections.emptyList();
        return newResultDTO(userRolePOS).failure();
    }*/

    
    private DefaultBeanHandler() {}
    
}
