/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.base.dto;

import java.io.Serializable;
import java.util.Collection;

import com.hyts.codex.base.vo.ResultVO;
import com.hyts.codex.config.ResultTypeEnum;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/** 
 * @author LiBo/Alex
 * @version V1.0
 * data transfer object to result 
 */
@Accessors(chain=true)
@NoArgsConstructor
public class ResultDTO<T extends ResultVO<?>> implements Serializable{

    /**  
     * @fieldName serialVersionUID
     * @fieldType long
     */ 
    private static final long serialVersionUID = 3272488659966284448L;
    
    @Getter
    private boolean success = true;
    
    @Getter
    @Setter
    private T result;
    
    @Getter
    @Setter
    private String message;
    
    @Getter
    private int code;
    
    /**  
     * @param data
     * @return
     * @exception
     */ 
    public ResultDTO<T> success(T data){
        this.success = true;
        this.message = ResultTypeEnum.SUCCESS.getMessage();
        this.code = ResultTypeEnum.SUCCESS.getCode();
        this.result = data;
        return this;
    }


    /**  
     * @return
     * @exception
     */ 
    public ResultDTO<T> success(){
        this.success = true;
        this.message = ResultTypeEnum.SUCCESS.getMessage();
        this.code = ResultTypeEnum.SUCCESS.getCode();
        return this;
    }
    
    /**  
     * @return
     * @exception
     */ 
    public ResultDTO<T> failure(ResultTypeEnum resultType){
        this.success = false;
        this.message = resultType.getMessage();
        this.code = resultType.getCode();
        return this;
    }
    
    
    /**  
     * @param message
     * @return
     * @exception
     */ 
    public ResultDTO<T> failure(String message){
        this.success = false;
        this.message = message;
        this.code = ResultTypeEnum.ERROR.getCode();
        return this;
    }
    
    /**  
     * @return
     * @exception
     */ 
    public ResultDTO<T> failure(){
        this.success = false;
        this.message = ResultTypeEnum.FAILURE.getMessage();
        this.code = ResultTypeEnum.FAILURE.getCode();
        return this;
    }
    
}
