/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.base.vo;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import lombok.Data;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 */
@Data
public class ResultVO<T> implements Serializable{

    /**  
     * @fieldName serialVersionUID
     * @fieldType long
     */ 
    private static final long serialVersionUID = 1L;
    
    /**
     * data sum num value
     */
    private Long dataTotal;
    
    /**
     * page sum num value
     */
    private Integer pageTotal;

    /**
     * data value
     */
    private T data;


    private List<T> dataList;




}
