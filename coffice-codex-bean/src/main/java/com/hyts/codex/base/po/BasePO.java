/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.base.po;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.hyts.codex.config.DataStatusEnum;

import lombok.Data;

/** 
 * @author LiBo/Alex
 * @version V1.0
 */
@Data
@MappedSuperclass
public abstract class BasePO<IDTYPE>{
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private IDTYPE id;
    
    @Column(name = "IS_DEL",length=2,columnDefinition="smallint default 3")
    private Byte isDelete;
    
    @Column(name = "CREATE_TIME")
    private Date createTime;
    
    @Column(name = "UPDATE_TIME")
    private Date updateTime;
    
    @Column(name = "CREATE_USER_ID")
    private IDTYPE createUserId;
    
    @Column(name = "UPDATE_USER_ID")
    private IDTYPE updateUserId;
    
    @Column(name = "STATUS",length=2,columnDefinition="smallint default 1")
    private Byte status;
    
}
