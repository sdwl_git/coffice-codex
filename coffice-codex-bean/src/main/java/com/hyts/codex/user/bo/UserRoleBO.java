package com.hyts.codex.user.bo;

import com.hyts.codex.base.bo.BaseBO;
import lombok.Data;

/**
 * project-name:coffice-codex
 * package-name:com.hyts.codex.user.bo
 * author:Libo/Alex
 * create-date:2019-04-27 20:16
 * copyright:libo-hyts-github
 * email:libo2dev@aliyun.com
 * description:此类主要用于: 主要用于操作用户角色数据传输对象的业务对象
 */

@Data
public class UserRoleBO extends BaseBO<Long> {

    /**
     * 角色id
     */
    Long roleId;

    /**
     * 角色编码-主要用于shiro的roles[中的数值]
     */
    String roleCode;

    /**
     * 客户端比较能够理解
     */
    String roleName;

    String userId;

    String userName;

}
