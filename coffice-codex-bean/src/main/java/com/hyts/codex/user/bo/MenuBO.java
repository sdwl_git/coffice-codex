package com.hyts.codex.user.bo;

import com.hyts.codex.base.bo.BaseBO;
import lombok.Data;

import javax.persistence.Column;

/**
 * project-name:coffice-codex
 * package-name:com.hyts.codex.user.bo
 * author:Libo/Alex
 * create-date:2019-04-28 21:08
 * copyright:libo-hyts-github
 * email:libo2dev@aliyun.com
 * description:此类主要用于: 菜单数据业务逻辑对象
 */
@Data
public class MenuBO extends BaseBO<Long> {

    private String menuCode;

    private String menuName;

    private String remark;

    private Long parentMenuId;

    private String menuIcon;

    private String menuUrl;

}
