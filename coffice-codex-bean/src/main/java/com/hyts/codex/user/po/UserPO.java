/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.user.po;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.hyts.codex.base.po.BasePO;

import lombok.Data;

/** 
 * @author LiBo/Alex
 * @see com.hyts.codex.bean.po.BasePO<Long>
 * @since V1.0
 * @version V1.0
 * User info Data model (用户信息数据模型)
 */
@Data
@Entity
@Table(name = "TBL_USER")
public class UserPO extends BasePO<Long>{
    
    @Column(name = "USER_NAME")
    private String userName;
    
    @Column(name = "PASSWORD")
    private String password;
    
    @Column(name = "REAL_NAME")
    private String realName;
    
    @Column(name = "EMAIL")
    private String email;
    
    @Column(name = "SALT")
    private String salt;
    
    @Column(name = "LAST_LOGIN_DATE")
    private Date lastLoginDate;
    
    /**  
     * @return
     * @exception
     */ 
    public String getCredentialsSalt() {
        return userName + salt;
    }
}
