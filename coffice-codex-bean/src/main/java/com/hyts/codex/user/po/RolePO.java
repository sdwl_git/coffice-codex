/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.user.po;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.hyts.codex.base.po.BasePO;

import lombok.Data;

/** 
 * @author LiBo/Alex
 * @see com.hyts.codex.bean.po.BasePO<Long>
 * @version V1.0 
 * organization data subject model
 */
@Data
@Entity
@Table(name = "TBL_ROLE")
public class RolePO extends BasePO<Long>{
    
    @Column(name = "ROLE_NAME")
    private String roleName;

    @Column(name = "ROLE_CODE")
    private String roleCode;
    
    @Column(name = "ORG_ID")
    private Long orgId;
    
    @Column(name = "REMARK")
    private String remark;

}
