/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.tool;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 */
public class NetworkUtils {
    
    /**  
     * @return
     * @exception
     */ 
    public static String getServerIpAddress() {
        String loopAddress = "127.0.0.1";
        try {
            Enumeration<NetworkInterface> netWorkAddressList = NetworkInterface.getNetworkInterfaces();
            while(netWorkAddressList.hasMoreElements()) {
                NetworkInterface networkInterface = netWorkAddressList.nextElement();
                Enumeration<InetAddress> ipaddressList = networkInterface.getInetAddresses();
                while(ipaddressList.hasMoreElements()) {
                    InetAddress address = ipaddressList.nextElement();
                    if(address != null && address instanceof Inet4Address && 
                            !address.isSiteLocalAddress() && !address.isLoopbackAddress()
                              && address.getHostAddress().indexOf(":") == -1) {
                        loopAddress =  address.getHostAddress();
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
            return loopAddress;
        }
        return loopAddress;
    }
    
}
