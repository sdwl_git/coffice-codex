/*
 * Copyright (c) 2016, LI BO/Alex All rights reserved.
 * 
 * http://blog.sina.com.cn/alex4java
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.tool;

//import static priv.libo.web.tools.FreemarkerUtil.pathType.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Locale;
import java.util.Map;


import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * @ClassName: FreemarkerFactory
 * @Description: Freemarker框架：<br>
 * 				 采用FreemarkerUtil工具框架进行 	
 * @author: 李博/Alex
 * @date: 2017-1-20 下午1:59:24
 */

enum pathType{ /*java源码级别 */ SOURCE_PATH,/*web服务级别*/CONTEXT_PATH;}
public final class FreemarkerUtil {
	
	/********************************定义系统所有相关的静态参数配置变量：**********************************************/
	
	/**
	 * @fieldName: FREEMARKER_ENCODE
	 * @fieldType: String
	 * @Description: Freemarker 编码：UTF-8
	 */
	private static final String FREEMARKER_ENCODE = "UTF-8";
	
	/********************************公共方法用于所有的方法进行调用操作：**********************************************/
	
	/**
	 * @Title: getSystemPath
	 * @Description: 获取系统服务路径
	 * @param type
	 * @return
	 * @return: String
	 */
	@SuppressWarnings("unused")
	private static String getPath(pathType type){
		if(type == null)
			throw new RuntimeException("传入的系统路径类型为空!");
		switch(type){
			case SOURCE_PATH:
				//获取本地dir操作：只局限与操作系统Windows以及配置过变量的其他操作系统
				String localUserDirPath = System.getProperty("user.dir");
				return localUserDirPath;
			case CONTEXT_PATH:
				 String contextPath = StringUtil.isEmpty(Thread.currentThread().getContextClassLoader().getResource("").getPath())?
						 Thread.currentThread().getContextClassLoader().getResource("").getPath():FreemarkerUtil.class.getClassLoader().getResource("").getFile();
				 return contextPath;
		}
		return "";
	} 
	
	/**
	 * @Title: createSourcePathTemplate
	 * @Description: 创建对应的源码路径：template
	 * @param templatePath
	 * @param templateName
	 * @return
	 * @return: Template
	 * @throws IOException 
	 */
	private static Template createSourcePathTemplate(String templatePath,String templateName) throws IOException{
		if(StringUtil.isEmpty(templatePath) || StringUtil.isEmpty(templateName) )
			throw new RuntimeException("传入的模板路径或者模板名称为空!");
		Configuration cfg = new Configuration();
		cfg.setEncoding(Locale.CHINA, FREEMARKER_ENCODE);
		cfg.setDirectoryForTemplateLoading(new File(getPath(pathType.SOURCE_PATH)+templateName));
		return cfg.getTemplate(templateName);
	}

	/**
	 * @Title: createContextPathTemplate
	 * @Description: 创建对应的项目路径：template
	 * @param templatePath
	 * @param templateName
	 * @return
	 * @return: Template
	 * @throws IOException 
	 */
	@SuppressWarnings("unused")
	private static Template createContextPathTemplate(String templatePath,String templateName) throws IOException{
		if(StringUtil.isEmpty(templatePath) || StringUtil.isEmpty(templateName) )
			throw new RuntimeException("传入的模板路径或者模板名称为空!");
		Configuration cfg = new Configuration();
		cfg.setEncoding(Locale.CHINA, FREEMARKER_ENCODE);
		cfg.setDirectoryForTemplateLoading(new File(getPath(pathType.CONTEXT_PATH)+templateName));
		return cfg.getTemplate(templateName);
	}
	
	/************************************执行Freemarker操作的路径方法操作***********************************************/
	/**
	 * @Title: printConsole
	 * @Description: 输出到控制台 用于测试
	 * @param templatePath
	 * @param templateName
	 * @param dataInfo
	 * @return: void
	 */
	@SuppressWarnings("unused")
	private static void printConsole(String templatePath,String templateName,Map<String,Object> dataInfo){
		try {
			Template template = createSourcePathTemplate(templatePath,templateName);
			template.process(dataInfo, new PrintWriter(System.out));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @Title: printOutFile
	 * @Description: 输出到文件用于生产代码或者文件用
	 * @param templatePath
	 * @param templateName
	 * @param dataInfo
	 * @param outFilePath
	 * @param outFileName
	 * @return: void
	 */
	@SuppressWarnings("unused")
	public static void printOutFile(String templatePath,String templateName,Map<String,Object> dataInfo,String outFilePath,String outFileName){
		try {
			//输出文件判断的方法
			if(StringUtil.isEmpty(outFilePath) && StringUtil.isEmpty(outFileName))
				throw new RuntimeException("传入输出文件路径以及文件名称为空!");
			File outFile = new File(FreemarkerUtil.getPath(pathType.CONTEXT_PATH)+outFilePath+outFileName);
			Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile),"UTF-8"));
			Template template = createSourcePathTemplate(templatePath,templateName);
			template.process(dataInfo, new PrintWriter(System.out));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}
	

}
