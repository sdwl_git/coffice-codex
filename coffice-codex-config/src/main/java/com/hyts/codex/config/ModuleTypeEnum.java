/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.config;

import java.util.Locale;

/**
 * @author LiBo/Alex
 * @version V1.0 
 * 异常类型信息
 */
public enum ModuleTypeEnum {


    INSTANCE;
    
    
    /**
     * LAYER 错误类型
     * @version V1.0
     */
    public enum LAYERErrorType{
        CONTROLLER,
        SERVICE,
        DAO,
        MODEL,
        DEFAULT;
    }
    
    /**
     * 操作访问层 错误类型
     * 上传或者下载
     * @version V1.0
     */
    public enum OPRErrorType{
        //增添
        ADD,
        //修改
        EDIT,
        //查询
        QUERY,
        //删除
        REMOVE,
        //上传
        UPLOAD,
        //下载
        DOWNLOAD,
        DEFAULT;

    }


    public String description(){
        return this.name().toLowerCase(Locale.CHINA);
    }
    
}
