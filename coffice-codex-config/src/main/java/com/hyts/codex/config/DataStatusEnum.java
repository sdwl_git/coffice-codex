/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.config;

import lombok.Getter;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 */
public enum DataStatusEnum {

    LOCKED("账号锁住",(byte)0),
    NORMAL("正常状态",(byte)1),
    IS_DEL("数据删除",(byte)2),
    NO_DEL("数据未删除",(byte)3);
    
    /**
     * 信息
     */
    @Getter
    private String message;
    /**
     * 编码
     */
    @Getter
    private Byte code;
    /**  
     * @constructor：ResultTypeEnum  
     * @param name
     * @param ordinal  
     */ 
    private DataStatusEnum(String name, Byte ordinal) {
        this.message = name;
        this.code = ordinal;
    }
    
    
}
