/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.config;

import lombok.Getter;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 * 结果类型
 */
public enum ResultTypeEnum {

    
    SUCCESS("操作成功",200),
    FAILURE("操作失败",500),
    ILLEGAL("token无效或者非法用户",401),
    TIMEOUT("系统超时或者Session超时",505),
    PASSWORD_NOPASS("密码错误",590),
    USER_NOFOUND("账号不存在系统",589),
    ACCOUNT_LOCK("账号已被锁定",591),
    ERROR("系统异常",999);
    
    /**
     * 信息
     */
    @Getter
    private String message;
    /**
     * 编码
     */
    @Getter
    private int code;
    /**  
     * @constructor：ResultTypeEnum  
     * @param name
     * @param ordinal  
     */ 
    private ResultTypeEnum(String name, int ordinal) {
        this.message = name;
        this.code = ordinal;
    }
    
    
}
