package com.hyts.codex.dao;

import com.hyts.codex.user.bo.UserRoleBO;
import com.hyts.codex.user.po.MenuPO;
import com.hyts.codex.user.po.RolePO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * project-name:coffice-codex
 * package-name:com.hyts.codex.dao
 * author:Libo/Alex
 * create-date:2019-04-28 20:54
 * copyright:libo-hyts-github
 * email:libo2dev@aliyun.com
 * description:此类主要用于: 菜单操作服务业务逻辑数据访问层次
 */
@Repository
public interface MenuRepository extends JpaRepository<MenuPO,Long> {


    /**
     * 根据RoleId相关信息服务查询相关的Menu菜单信息
     * @return
     */
    @Query(value="select distinct tm from MenuPO tm left join " +
            "RoleMenuPO trm on tm.id = trm.menuId " +
            "left join RolePO tr on tr.id = trm.roleId " +
            "where tr.id in(:roleids)"/*,nativeQuery=true*/)
    List<MenuPO> findByRoleIds(@Param("roleids") List<Long> roleIds);

}
