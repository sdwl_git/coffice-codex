package com.hyts.codex.dao;

import com.hyts.codex.user.po.RolePO;
import com.hyts.codex.user.po.UserPO;
import com.hyts.codex.user.po.UserRolePO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * project-name:coffice-codex
 * package-name:com.hyts.codex.dao
 * author:Libo/Alex
 * create-date:2019-04-27 20:36
 * copyright:libo-hyts-github
 * email:libo2dev@aliyun.com
 * description:此类主要用于: 用户权限角色信息操作数据服务
 */
@Repository
public interface UserRoleRepository extends QueryByExampleExecutor<UserRolePO>,JpaRepository<UserRolePO,Long> {

    List<UserRolePO> findByUserId(Long userId);

    List<UserRolePO> findByRoleId(Long roleId);

/*
    UserRolePO findByRoleCode(String roleCode);
*/

}
