package com.hyts.codex.dao;

import com.hyts.codex.user.po.RoleMenuPO;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * project-name:coffice-codex
 * package-name:com.hyts.codex.dao
 * author:Libo/Alex
 * create-date:2019-04-28 20:56
 * copyright:libo-hyts-github
 * email:libo2dev@aliyun.com
 * description:此类主要用于: 角色菜单对于的关系表数据访问层次
 */
public interface RoleMenuRepository extends JpaRepository<RoleMenuPO,Long> {



}
