package com.hyts.codex.dao;

import com.hyts.codex.user.po.RolePO;
import com.hyts.codex.user.po.UserPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * project-name:coffice-codex
 * package-name:com.hyts.codex.dao
 * author:Libo/Alex
 * create-date:2019-04-27 22:54
 * copyright:libo-hyts-github
 * email:libo2dev@aliyun.com
 * description:此类主要用于: 角色信息对象的查询
 */
public interface RoleRepository extends JpaRepository<UserPO,Long> {


    @Query(value="SELECT r FROM UserPO user INNER JOIN UserRolePO ur ON user.id = ur.userId INNER JOIN RolePO r ON r.id = ur.roleId WHERE user.id = ?1")
    List<RolePO> findRoleByUserId(Long userId);

}
