package com.hyts.codex.service.impl;

import com.hyts.codex.base.DefaultBeanHandler;
import com.hyts.codex.base.dto.ResultDTO;
import com.hyts.codex.base.vo.ResultVO;
import com.hyts.codex.common.api.IMenuService;
import com.hyts.codex.dao.MenuRepository;
import com.hyts.codex.user.bo.MenuBO;
import com.hyts.codex.user.po.MenuPO;
import com.hyts.codex.user.po.RolePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.awt.*;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * project-name:coffice-codex
 * package-name:com.hyts.codex.service.impl
 * author:Libo/Alex
 * create-date:2019-04-28 21:12
 * copyright:libo-hyts-github
 * email:libo2dev@aliyun.com
 * description:此类主要用于: 菜单权限查询操作方法服务功能
 */
@Service
public class MenuService implements IMenuService {


    @Autowired
    MenuRepository menuRepository;

    /**
     * 查询菜单根据roleIds服务
     * @param roleIds
     * @return
     */
    @Override
    public ResultDTO<ResultVO<MenuBO>> findMenuByRoleIds(List<Long> roleIds) {
        List<MenuPO> menuPOS = menuRepository.findByRoleIds(roleIds);
        //菜单对象服务数据为空！
        if(CollectionUtils.isEmpty(menuPOS)){
           return DefaultBeanHandler.getINSTANCE().returnFailure(MenuBO.class);
        }
        List<MenuBO> menuBOS = menuPOS.parallelStream().map(param->{
            MenuBO menuBOS1 = new MenuBO();
            BeanUtils.copyProperties(param,menuBOS1);
            return menuBOS1;
        }).collect(Collectors.toList());
        return DefaultBeanHandler.getINSTANCE().returnListSuccess(menuBOS,MenuBO.class);
    }
}
