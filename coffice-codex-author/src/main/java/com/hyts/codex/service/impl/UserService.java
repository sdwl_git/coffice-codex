/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.service.impl;

import java.util.Objects;
import java.util.Optional;

import com.hyts.codex.config.DataStatusEnum;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hyts.codex.base.DefaultBeanHandler;
import com.hyts.codex.base.dto.ResultDTO;
import com.hyts.codex.base.vo.ResultVO;
import com.hyts.codex.common.api.IUserService;
import com.hyts.codex.dao.UserRepository;
import com.hyts.codex.user.bo.UserBO;
import com.hyts.codex.user.dto.LoginDTO;
import com.hyts.codex.user.po.UserPO;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 */
@Transactional
@Service("userService")
public class UserService implements IUserService{
    
    
    @Autowired
    UserRepository userRepository;
    
    /**  
     * @param loginDTO
     * @return
     * @exception
     */
    @Override
    public ResultDTO<ResultVO<UserBO>> login(LoginDTO loginDTO){
       UserPO userPO = userRepository.findByUserName(loginDTO.getUserName());
       if(userPO == null) {
          return DefaultBeanHandler.getINSTANCE().returnFailure(UserBO.class);
       }
       UserBO userBo = new UserBO();
       //进行赋值初始化BO数据对象
       userBo.setPassword(userPO.getPassword());
       userBo.setCredentialsSalt(userPO.getCredentialsSalt());
       userBo.setStatus(userPO.getStatus());
       return DefaultBeanHandler.getINSTANCE().returnSuccess(userBo);
    }
    
    /**  
     * @return
     * @exception
     */
    @Override
    public ResultDTO<ResultVO<UserBO>> register(UserBO bo){
        if(Objects.isNull(bo)) {
            throw new NullPointerException("注册信息不可以为空!");
        }
        UserPO userPO = userRepository.findByUserName(bo.getUserName());
        if(userPO != null) {
           return DefaultBeanHandler.getINSTANCE().returnFailure(UserBO.class).setMessage("账号已存在！");
        }
        userPO = new UserPO();
        BeanUtils.copyProperties(bo, userPO);
        userPO.setStatus((byte) DataStatusEnum.NORMAL.getCode());
        return Optional.ofNullable(userRepository.save(userPO)).isPresent()?
                DefaultBeanHandler.getINSTANCE().returnSuccess(UserBO.class):
                DefaultBeanHandler.getINSTANCE().returnFailure(UserBO.class);
    }


    /**
     * 查询加载用户ID根据用户名称
     * @param userName
     * @return
     */
    @Override
    public ResultDTO<ResultVO<UserBO>> loadUserIdByUserName(String userName) {
        UserPO userPO = userRepository.findByUserName(userName);
        if(userPO == null) {
            return DefaultBeanHandler.getINSTANCE().returnFailure(UserBO.class);
        }
        UserBO userBO = new UserBO();
        BeanUtils.copyProperties(userPO,userBO);
        return DefaultBeanHandler.getINSTANCE().returnSuccess(userBO);
    }


}
