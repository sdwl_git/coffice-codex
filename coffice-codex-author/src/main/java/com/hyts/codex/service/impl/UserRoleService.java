package com.hyts.codex.service.impl;

import com.hyts.codex.base.DefaultBeanHandler;
import com.hyts.codex.base.dto.ResultDTO;
import com.hyts.codex.base.vo.ResultVO;
import com.hyts.codex.common.api.IUserRoleService;
import com.hyts.codex.dao.RoleRepository;
import com.hyts.codex.dao.UserRoleRepository;
import com.hyts.codex.user.bo.UserRoleBO;
import com.hyts.codex.user.po.RolePO;
import com.hyts.codex.user.po.UserPO;
import com.hyts.codex.user.po.UserRolePO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * project-name:coffice-codex
 * package-name:com.hyts.codex.service.impl
 * author:Libo/Alex
 * create-date:2019-04-27 20:31
 * copyright:libo-hyts-github
 * email:libo2dev@aliyun.com
 * description:此类主要用于: 用与查询相关的用户角色信息数据对象
 */
@Service
public class UserRoleService implements IUserRoleService {


    @Autowired
    UserRoleRepository userRoleRepository;


    @Autowired
    RoleRepository roleRepository;

    @Override
    public ResultDTO<ResultVO<UserRoleBO>> findRoleDataByUserId(Long userId) {
        List<RolePO> userRolePO = roleRepository.findRoleByUserId(userId);
        if(Optional.ofNullable(userRolePO).isPresent()){
            List<UserRoleBO> userRoleBOS = new ArrayList<>();
            for(RolePO po:userRolePO){
                UserRoleBO bo = new UserRoleBO();
                BeanUtils.copyProperties(po,bo);
                bo.setRoleId(po.getId());
                userRoleBOS.add(bo);
            }
           return DefaultBeanHandler.getINSTANCE().returnListSuccess(userRoleBOS,UserRoleBO.class);
        }else{
            return DefaultBeanHandler.getINSTANCE().returnFailure(UserRoleBO.class);
        }
    }

    @Override
    public ResultDTO<ResultVO<UserRoleBO>> findRoleDataByRoleId(Long roleId) {
        return null;
    }

    @Override
    public ResultDTO<ResultVO<UserRoleBO>> findRoleDataByRoleCode(String roleCode) {
        return null;
    }
}
