/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.redis.service;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 * cache handler function
 */
public interface ICacheApi<R> {
    
    //get key by value
    R get(String key);
    
    //put key and value
    void put(String key,R value);
    
    //put key and value expireTime
    void put(String key,R value,int expireTime);
     
    //setting the expire time
    void setExpire(String key,int expireTime);
    
    //get expiretime operation
    Long getExpire(String key);
    
    //remove the object
    boolean remove(String key);
    
    //remove all object
    void removeAll();
}
