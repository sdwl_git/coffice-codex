/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.redis.service;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 * redis lock function
 */
public interface ILockApi {
     
    
    /**
     * s - (seconds) default lock expire time
     */
    int DEFAULT_LOCK_EXPIRE_TIME = 10;
    
    /**
     * s - (seconds) default lock time out value
     */
    int DEFAULT_LOCK_TIME_OUT = 10;
    
    
    String DEFAULT_LOCK_KEY_PREFIX = "REDIS_LOCK_";
    
    
    String REDIS_RESULT_SUCCESS = "OK";
    

    String DEFAULT_LOCK_NX = "NX",DEFAULT_LOCK_EX= "EX";
    
    
    Long RELEASE_SUCCESS = 1L;
    
    
    /**
     * lock data
     * @param key
     * @return
     * @exception
     */
    String lock(String key);
    
    /**
     * try to do get lock
     * @param key
     * @return
     * @exception
     */
    String tryLock(String key);
    
    /**
     * try to do get lock
     * @param key
     * @param timeout
     * @return
     * @exception
     */
    String tryLock(String key,int timeout);
    
    /**
     * unlock the lock data
     * @param key
     * @param value
     * @return
     * @exception
     */
    boolean unLock(String key, String value);

}
