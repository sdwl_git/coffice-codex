/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.redis.service;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import redis.clients.jedis.Jedis;

/** 
 * @author LiBo/Alex
 * @version V1.0 
 */
public interface IDGenApi {
       
    /**
     * default id token split token word
     */
    String DEFAULT_SPLIT_TOKEN = "-";
    
    int DEFAULT_ID_LENGTH = 8;
    
    boolean DEFAULT_JOIN_DAY_HEAD = true;
    
    String DEFAULT_DATE_FORMAT = "yyyyMMdd";
     
    /**
     * get id interval day
     * @return
     * @exception
     */
    Long genIdDaily(String key);
    
    /**
     * get id interval day
     * @return
     * @exception
     */
    Long genIdDaily(String key,Boolean haveDay);
    
    /**
     * get id interval day
     * @return
     * @exception
     */
    Long genIdDaily(String key, Integer length, Boolean haveDay);
    
    /**
     * get id all day
     * @return
     * @exception
     */
    Long genId(String key);
    
    /**
     * get id all day
     * @return
     * @exception
     */
    Long genId(String key, Integer length);
    
    /**
     * get id all day
     * @return
     * @exception
     */
    Long genId(String key, Integer length, Boolean haveDay);
    
    
    
    //default method to format the id value number
    default Long format(Long num,int length,String day) {
        String id = String.valueOf(num);
        if (id.length() < length) {
            NumberFormat nf = NumberFormat.getInstance();
            nf.setGroupingUsed(false);
            nf.setMaximumIntegerDigits(length);
            nf.setMinimumIntegerDigits(length);
            id = nf.format(num);
        }
        return Long.parseLong(day + id);
    }
    
    
}
