/* 
 * Copyright [2018] [Alex/libo(liboware@gmail.com)]
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.codex.redis.service.impl;

import java.util.Objects;

import com.hyts.codex.redis.model.JedisConnection;
import com.hyts.codex.redis.service.ICacheApi;

import lombok.AllArgsConstructor;
import redis.clients.jedis.Jedis;

/** 
 * @author LiBo/Alex
 * @see com.hyts.ssm.api.ICacheApi
 * @version V1.0 
 */
@AllArgsConstructor
public abstract class RCacheService implements ICacheApi<Object>{
    
    
    private final JedisConnection connection;
    
    private final Integer dbIndex;
    
    /**
     * 序列化
     * @param param
     * @return
     * @exception
     */
    public abstract byte[] serial(Object param);
    
    /**
     * 反序列化
     * @param param
     * @return
     * @exception
     */
    public abstract Object deserial(byte[] param);

    /* (非 Javadoc)
     * @param key
     * @return
     * @see com.hyts.ssm.api.ICacheApi#get(java.lang.String)  
     * @exception
     */ 
    @Override
    public Object get(String key) {
        Jedis session = connection.openSession().orElse(connection.openSession().get());
        session.select(dbIndex);
        byte[] value;
        try {
            value = session.get(key.getBytes());
            if (Objects.isNull(value)) {
                return null;
            } 
        } finally {
            if(!Objects.isNull(session)) {
                session.close();
            }
        }
        return deserial(value);
    }

    /* (非 Javadoc)
     * @param key
     * @param value
     * @see com.hyts.ssm.api.ICacheApi#put(java.lang.String, java.lang.Object)  
     * @exception
     */ 
    @Override
    public void put(String key, Object value) {
        put(key,value,-1);
    }

    /* (非 Javadoc)
     * @param key
     * @param value
     * @param expireTime
     * @see com.hyts.ssm.api.ICacheApi#put(java.lang.String, java.lang.Object, int)  
     * @exception
     */ 
    
    @Override
    public void put(String key, Object value, int expireTime) {
        Jedis session = connection.openSession().orElse(connection.openSession().get());
        session.select(dbIndex);
        try {
            if (expireTime <= 0)
                session.set(key.getBytes(), serial(value));
            else
                session.setex(key.getBytes(), expireTime, serial(value));
        } finally {
            if(!Objects.isNull(session)) {
                session.close();
            }
        }
    }

    /* (非 Javadoc)
     * <b>TODO(方法功能的描述)-  setExpire </b>
     * <p>TODO(这里用一句话描述这个方法的作用)</p>
     * @param key
     * @param expireTime
     * @see com.hyts.ssm.api.ICacheApi#setExpire(java.lang.String, int)  
     * @exception
     */ 
    
    @Override
    public void setExpire(String key, int expireTime) {
        
    }

    /* (非 Javadoc)
     * <b>TODO(方法功能的描述)-  getExpire </b>
     * <p>TODO(这里用一句话描述这个方法的作用)</p>
     * @param key
     * @return
     * @see com.hyts.ssm.api.ICacheApi#getExpire(java.lang.String)  
     * @exception
     */ 
    
    @Override
    public Long getExpire(String key) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (非 Javadoc)
     * <b>TODO(方法功能的描述)-  remove </b>
     * <p>TODO(这里用一句话描述这个方法的作用)</p>
     * @param key
     * @return
     * @see com.hyts.ssm.api.ICacheApi#remove(java.lang.String)  
     * @exception
     */ 
    
    @Override
    public boolean remove(String key) {
        // TODO Auto-generated method stub
        return false;
    }

    /* (非 Javadoc)
     * @see com.hyts.ssm.api.ICacheApi#removeAll()  
     * @exception
     */ 
    
    @Override
    public void removeAll() {
        
    }

    public RCacheService(JedisConnection connection) {
        super();
        this.connection = connection;
        this.dbIndex = 0;
    }
    
}
