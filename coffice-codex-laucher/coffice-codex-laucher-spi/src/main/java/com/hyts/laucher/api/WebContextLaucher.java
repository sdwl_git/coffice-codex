/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.laucher.api;

import org.springframework.context.ConfigurableApplicationContext;

/**
 * @project-name:coffice-codex
 * @package-name:com.hyts.codex.multilaucher
 * @author:LiBo/Alex
 * @create-date:2019-10-17 00:10
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */

public interface WebContextLaucher {

    /**
     * 名称标识系统名称用与区分
     * @return
     */
    String name();
    /**
     * 服务启动编号，用于排序多个项目启动的时候的顺序功能
     * @return
     */
    int order();

    /**
     * 服务启动方法操作服务-所有服务启动的入口功能实现
     * @return
     */
    boolean run(String[] args, ConfigurableApplicationContext parentApplicationContext);

}
