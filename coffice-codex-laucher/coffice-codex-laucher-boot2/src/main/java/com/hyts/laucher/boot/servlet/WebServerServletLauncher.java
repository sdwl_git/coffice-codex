/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.laucher.boot.servlet;

import com.hyts.laucher.api.WebContextLaucher;
import com.hyts.laucher.boot.ServerBootLaucher;
import org.springframework.boot.Banner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

import java.lang.reflect.Modifier;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.ServiceLoader;

/**
 * @project-name:coffice-codex
 * @package-name:com.hyts.laucher.boot.servlet
 * @author:LiBo/Alex
 * @create-date:2019-10-17 14:19
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */

public abstract class WebServerServletLauncher extends ServerBootLaucher {

    public abstract int port();

    public abstract String contextPath();

}
