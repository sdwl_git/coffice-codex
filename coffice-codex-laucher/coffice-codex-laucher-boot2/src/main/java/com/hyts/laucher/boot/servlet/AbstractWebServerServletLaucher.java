/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.laucher.boot.servlet;

import com.hyts.laucher.api.WebContextLaucher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.Banner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.server.WebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @project-name:coffice-codex
 * @package-name:com.hyts.laucher.boot
 * @author:LiBo/Alex
 * @create-date:2019-10-17 13:45
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@Slf4j
public abstract class AbstractWebServerServletLaucher<T extends WebServerFactory>  extends WebServerServletLauncher implements WebContextLaucher, WebServerFactoryCustomizer<T> {

    /**
     * 创建委托代理服务对象
     * @param application 应用启动类-被代理类
     * @param configurationApplicationContext 应用上下文
     * @param args 传入数据参数功能实现
     * @return
     */
    public ConfigurableApplicationContext createDelegrateServer(Class<?> application,ConfigurableApplicationContext
            configurationApplicationContext,String[] args){
        /*            AnnotationConfigServletWebServerApplicationContext springApplication = (
              AnnotationConfigServletWebServerApplicationContext) serverApplicationContext;*/
        ConfigurableApplicationContext condigurationApplicationContext = new SpringApplicationBuilder(application).
                    web(WebApplicationType.SERVLET).
                    bannerMode(Banner.Mode.OFF).
                    parent(configurationApplicationContext).run(args);
            log.info("{} 文件服务子系统是否处于活跃状态 : {}",condigurationApplicationContext.getId()
                ,condigurationApplicationContext.isActive());
            return condigurationApplicationContext;
    }

}
