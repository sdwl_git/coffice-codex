/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.laucher.boot;

import com.hyts.laucher.api.WebContextLaucher;
/*
import lombok.extern.slf4j.Slf4j;
*/
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.Banner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.lang.reflect.Modifier;
import java.util.*;
import java.util.logging.Logger;

/**
 * @project-name:coffice-codex
 * @package-name:com.hyts.laucher.api
 * @author:LiBo/Alex
 * @create-date:2019-10-17 14:39
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */


@Slf4j
@SpringBootApplication
public class ServerBootLaucher /*extends WebMvcConfigurationSupport */{


    public static void main(String[] args){
        System.setProperty("spring.devtools.restart.enabled","false");
        log.info("开始启动加载boot本身（{}）启动服务上下文功能！",ServerBootLaucher.class.getSimpleName());
        //自身的服务启动
        ConfigurableApplicationContext parentApplicationContext =
                new SpringApplicationBuilder(WebContextLaucher.class).
                        web(WebApplicationType.NONE).run(args);
        //如果进行排序的话
        List<WebContextLaucher> dataApplicationList = new LinkedList<>();
        ServiceLoader.
                load(WebContextLaucher.class,
                        WebContextLaucher.class.getClassLoader()).forEach(param->{
            if(!param.getClass().isInterface() &&
                    !Modifier.isAbstract(param.getClass().getModifiers())){
                dataApplicationList.add(param);
            }
        });
        dataApplicationList.parallelStream().sorted(Comparator.comparingInt(WebContextLaucher::order))
                .forEach(service->{
                    log.info("------------------------------------------------------------------");
                    log.info("【 ---- 开始启动子系统:{} ---- 】",service.name());
                    log.info("------------------------------------------------------------------");
                    service.run(args, parentApplicationContext);
                });
        log.info("完成启动加载boot本身（{}）启动服务上下文功能！",ServerBootLaucher.class.getSimpleName());
    }

   /* @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static").addResourceLocations("classpath:/static/")
        super.addResourceHandlers(registry);
    }*/
}
